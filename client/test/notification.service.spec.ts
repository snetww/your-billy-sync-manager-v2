import { TestBed } from "@angular/core/testing";
import { ToastrModule, ToastrService } from "ngx-toastr";
import { NotificationService } from 'src/app/common/services/notification.service';


describe("NotificationService", () => {
  const toastr = {
    success() {},
    warning() {},
    error() {},
  };

  let service: NotificationService;

  beforeEach(() => {
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      imports: [ToastrModule],
      providers: [NotificationService],
    }).overrideProvider(ToastrService, { useValue: toastr });

    service = TestBed.get(NotificationService);
  });

  it("success should call success", () => {
    const message = "test";
    const title = "Yes"
    const spy = jest.spyOn(toastr, "success").mockImplementation(() => {});

    service.success(message, title);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith(message, title);
  });

  it("warn should call warning", () => {
    const message = "test";
    const spy = jest.spyOn(toastr, "warning").mockImplementation(() => {});
    const title = "Yes"
    service.warning(message, title);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith(message, title);
  });

  it("error should call error", () => {
    const message = "test";
    const spy = jest.spyOn(toastr, "error").mockImplementation(() => {});
    const title = "No"
    service.error(message, title);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith(message, title);
  });
});
