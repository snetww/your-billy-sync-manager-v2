import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from 'src/app/common/config';

@Injectable({
  providedIn: 'root',
})
export class ContactsService {
  public readonly showContactSubject$ = new Subject<any>();
  constructor(private readonly httpClient: HttpClient) {}

  public getAllContacts(
    userId: string,
    sortOrder = 'ASC',
    limit: number,
    offset: number
  ): void {
    this.httpClient
      .get(
        `${CONFIG.DOMAIN_NAME}/contact/contacts/${userId}/${sortOrder}/${limit}/${offset}`
      )
      .subscribe((data) => this.showContactSubject$.next(data as any));
  }
}
