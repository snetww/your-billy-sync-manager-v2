import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllContactsComponent } from './all-contacts/all-contacts.component';
import { SingleContactsComponent } from './single-contacts/single-contacts.component';
import { MaterialModule } from 'src/app/material/material.module';




@NgModule({
  declarations: [AllContactsComponent, SingleContactsComponent],
  imports: [CommonModule, MaterialModule],
})
export class ContactsModule {}
