import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-single-contacts',
  templateUrl: './single-contacts.component.html',
  styleUrls: ['./single-contacts.component.css'],
})
export class SingleContactsComponent implements OnInit {
  @Input()
  public contact: any;
  constructor() {}

  ngOnInit(): void {}
}
