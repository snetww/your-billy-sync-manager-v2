import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/common/services/storage.service';
import { ContactsService } from '../contacts.service';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-all-contacts',
  templateUrl: './all-contacts.component.html',
  styleUrls: ['./all-contacts.component.css'],
})
export class AllContactsComponent implements OnInit {
  contacts: any = [];
  userId: string;
  pageSize: number = 25;
  pageSizeOptions: number[] = [1, 5, 10, 25, 100];
  sortValue = 'ASC';
  currentPage: number = 1;
  totalCount: number;
  constructor(
    private readonly storageService: StorageService,
    private readonly contactService: ContactsService
  ) {
    this.userId = this.storageService.getItem('userId');
  }

  ngOnInit(): void {
    const res = this.contactService.showContactSubject$.subscribe((data) => {
      this.contacts = data.contacts;
      this.totalCount = data.count;
    });

    this.getData();
  }

  getData(): void {
    this.contactService.getAllContacts(
      this.userId,
      this.sortValue,
      this.pageSize,
      this.currentPage
    );
  }
  sort(event): void {
    this.sortValue = this.sortValue === 'ASC' ? 'DESC' : 'ASC';
    this.getData();
  }

  onChangePage(event: PageEvent) {
    this.pageSize = event.pageSize;
    this.currentPage = event.pageIndex;
    this.getData();
  }
}
