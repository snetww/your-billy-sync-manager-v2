import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-single-products',
  templateUrl: './single-products.component.html',
  styleUrls: ['./single-products.component.css'],
})
export class SingleProductsComponent implements OnInit {
  @Input()
  public product: any;
  constructor() {}

  ngOnInit(): void {}
}
