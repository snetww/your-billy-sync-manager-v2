import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SingleProductsComponent } from './single-products/single-products.component';
import { AllProductsComponent } from './all-products/all-products.component';
import { MaterialModule } from 'src/app/material/material.module';



@NgModule({
  declarations: [AllProductsComponent, SingleProductsComponent],
  imports: [
    CommonModule,
    MaterialModule
  ]
})
export class ProductsModule { }
