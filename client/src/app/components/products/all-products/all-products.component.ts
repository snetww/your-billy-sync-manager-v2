import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { StorageService } from '../../../common/services/storage.service';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-all-products',
  templateUrl: './all-products.component.html',
  styleUrls: ['./all-products.component.css'],
})
export class AllProductsComponent implements OnInit {
  products: any = [];
  pageSize: number = 25;
  pageSizeOptions: number[] = [1, 5, 10, 25, 100];
  sortValue = 'ASC';
  userId: string;
  currentPage: number = 1;
  totalCount: number;
  constructor(
    private readonly productService: ProductService,
    private readonly storageService: StorageService
  ) {
    this.userId = this.storageService.getItem('userId');
  }

  ngOnInit(): void {
    const res = this.productService.showProductSubject$.subscribe((data) => {
      console.log(data)
      this.products = data.products;
      this.totalCount = data.count;
    });

    this.getData();
  }

  getData(): void {
    this.productService.getAllProducts(
      this.userId,
      this.sortValue,
      this.pageSize,
      this.currentPage
    );
  }

  sort(event): void {
    this.sortValue = this.sortValue === 'ASC' ? 'DESC' : 'ASC';
    this.getData();
  }

  onChangePage(event: PageEvent) {
    this.pageSize = event.pageSize;
    this.currentPage = event.pageIndex;
    this.getData();
  }
}
