import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from 'src/app/common/config';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  public readonly showProductSubject$ = new Subject<any>();
  constructor(private readonly httpClient: HttpClient) {}

   getAllProducts(userId: string, sortOrder = 'ASC', limit: number, offset: number): void {
    this.httpClient
      .get(`${CONFIG.DOMAIN_NAME}/products/${userId}/${sortOrder}/${limit}/${offset}`)
      .subscribe((data) => this.showProductSubject$.next(data as any));
  }
}

