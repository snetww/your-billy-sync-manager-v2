import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/common/services/notification.service';
import { AddService } from './add.service';
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
})
export class AddComponent implements OnInit {
  public productForm: FormGroup;
  public contactForm: FormGroup;
  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly notification: NotificationService,
    private readonly router: Router,
    private readonly addService: AddService
  ) {}

  ngOnInit(): void {
    this.productForm = this.formBuilder.group({
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(40),
        ],
      ],
      vendor: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(40),
        ],
      ],
      version: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(20),
        ],
      ],
    });

    this.contactForm = this.formBuilder.group({
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(30),
        ],
      ],
      phone: [
        '',
        [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(10),
        ],
      ],
      address: [
        '',
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(80),
        ],
      ],
    });
  }
  public addContact(name: string, phone: string, address: string): any {
    return this.addService.addContact(name, phone, address).subscribe(
      () => {
        this.notification.success('Contact Added!');
        this.router.navigate(['/contacts']);
      },
      () => this.notification.error('Something went wrong!')
    );
  }
  public addProduct(name: string, vendor: string, version: string): any {
    return this.addService.addProduct(name, vendor, version).subscribe(
      () => {
        this.notification.success('Product Added!');
        this.router.navigate(['/products']);
      },
      () => this.notification.error('Something went wrong!')
    );
  }
}
