import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CONFIG } from 'src/app/common/config';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AddService {
  constructor(private readonly httpClient: HttpClient) {}

  public addProduct(
    name: string,
    vendor: string,
    version: string
  ): Observable<any> {
    return this.httpClient.post(`${CONFIG.DOMAIN_NAME}/products`, {
      name,
      vendor,
      version,
    });
  }

  public addContact(
    name: string,
    phone: string,
    address: string
  ): Observable<any> {
    return this.httpClient.post(`${CONFIG.DOMAIN_NAME}/contact`, {
      name,
      phone,
      address,
    });
  }
}
