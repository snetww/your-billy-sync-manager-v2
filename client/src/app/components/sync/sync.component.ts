import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/common/services/notification.service';
import { SyncService } from './sync.service';

@Component({
  selector: 'app-sync',
  templateUrl: './sync.component.html',
  styleUrls: ['./sync.component.css'],
})
export class SyncComponent implements OnInit {
  clicked: boolean = false;
  constructor(
    private readonly syncService: SyncService,
    private readonly notification: NotificationService
  ) {}

  ngOnInit(): void {}

  sync(): void {
    this.syncService.syncAllProductsAndContacts().subscribe(
      () => {
        this.notification.success('Syncing started...');
      },
      () => this.notification.error('Something went wrong!')
    );
  }
}
