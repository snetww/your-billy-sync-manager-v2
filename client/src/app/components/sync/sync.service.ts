import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from 'src/app/common/config';
import { StorageService } from 'src/app/common/services/storage.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SyncService {
  userId: string;
  constructor(
    private readonly httpClient: HttpClient,
    private readonly storageService: StorageService
  ) {
    this.userId = this.storageService.getItem('userId');
  }

  syncAllProductsAndContacts(): Observable<any> {
    return this.httpClient.get(`${CONFIG.DOMAIN_NAME}/billy/add/${this.userId}`);
  }
}
