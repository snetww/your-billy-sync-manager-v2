import { UsersService } from './users.service';
import { NgModule } from '@angular/core';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UsersRoutingModule } from './users-routing.module';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';




@NgModule({
  declarations: [
    UserProfileComponent,
  ],
  imports: [
    UsersRoutingModule,
    FormsModule,
    MaterialModule,
    InfiniteScrollModule
  ],
  providers: [UsersService],
})
export class UsersModule { }
