export class LoggedInUserDTO {

    public id: number;

    public username: string;

    public email: string;

}
