import { SyncComponent } from './components/sync/sync.component';
import { RegisterComponent } from './auth/register/register.component';
import { LoginComponent } from './auth/login/login.component';
import { ServerErrorComponent } from './components/server-error/server-error.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AuthGuard } from './auth/auth.guard';
import { AllProductsComponent } from './components/products/all-products/all-products.component';
import { AllContactsComponent } from './components/contacts/all-contacts/all-contacts.component';
import { AddComponent } from './components/add/add.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'auth/login', component: LoginComponent },
  { path: 'auth/register', component: RegisterComponent },
  {
    path: 'products',
    component: AllProductsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'contacts',
    component: AllContactsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'add',
    component: AddComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'sync',
    component: SyncComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'users',
    loadChildren: () =>
      import('./users/users.module').then((m) => m.UsersModule),
  },
  { path: 'not-found', component: NotFoundComponent },
  { path: 'server-error', component: ServerErrorComponent },
  { path: '**', redirectTo: '/not-found' },
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
