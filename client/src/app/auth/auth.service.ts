import { LoggedInUserDTO } from '../users/models/logged-in-user.dto';
import { StorageService } from './../common/services/storage.service';
import { CONFIG } from './../common/config';
import { Observable, BehaviorSubject } from 'rxjs';
import { LoginUserDTO } from './../users/models/login-user.dto';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly isLoggedInSubject$ = new BehaviorSubject<boolean>(this.isUserLoggedIn());
  private readonly loggedUserSubject$ = new BehaviorSubject<LoggedInUserDTO>(this.loggedUser());

  constructor(
    private readonly httpClient: HttpClient,
    private readonly storageService: StorageService,
    private readonly jwtService: JwtHelperService,
    private readonly router: Router
  ) { }

  public get isLoggedIn$(): Observable<boolean> {

    return this.isLoggedInSubject$.asObservable();
  }

  public get loggedUser$(): Observable<LoggedInUserDTO> {

    return this.loggedUserSubject$.asObservable();
  }

  public login(user: LoginUserDTO): Observable<any> {
    return this.httpClient
      .post<LoginUserDTO>(`${CONFIG.DOMAIN_NAME}/login`, user)
      .pipe(tap(({ accessToken, userId }) => {
        this.storageService.setItem('userId', userId);
        const loggedUser = this.jwtService.decodeToken(accessToken);

        this.storageService.setItem('token', accessToken);

        this.isLoggedInSubject$.next(true);
        this.loggedUserSubject$.next(loggedUser);
      }));
  }

  public getUserDataIfAuthenticated(): LoggedInUserDTO {
    const token: string = this.storageService.getItem('token');

    if (token && this.jwtService.isTokenExpired(token)) {
      this.storageService.removeItem('token');
      this.storageService.removeItem('userId');
      return null;
    }

    return token ? this.jwtService.decodeToken(token) : null;
  }

  public isUserLoggedIn(): boolean {
    return !!this.storageService.getItem('token');
  }

  private loggedUser(): LoggedInUserDTO {
    try {
      return this.jwtService.decodeToken(this.storageService.getItem('token'));
    } catch (error) {
      this.isLoggedInSubject$.next(false);

      return null;
    }
  }

  public registerUser(username: string, password: string, email: string): Observable<any> {
    return this.httpClient.post(`${CONFIG.DOMAIN_NAME}/register`, {username, password, email});
  }

  public logOutUser() {
    this.storageService.removeItem('token');
    this.storageService.removeItem('userId');
    this.router.navigate(['/home']);
  }
}
