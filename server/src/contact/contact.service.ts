import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Contact } from 'src/database/entities/contact.entity';
import { ShowContactDTO } from './models/show-contact.dto';
import { User } from '../database/entities/user.entity';
import { CreateContactDTO } from './models/create-contact.dto';
import { plainToClass } from 'class-transformer';
@Injectable()
export class ContactService {
  constructor(
    @InjectRepository(Contact)
    private readonly contactRepository: Repository<Contact>,
  ) {}

  public async getUserContacts(
    userId: string,
    sort: string,
    limit: number,
    offset: number,
  ): Promise<any> {
    const cnt = await this.contactRepository.count({
      where: { user: userId, isDeleted: false },
    });
    offset = offset - 1 < 0 ? 0 : offset - 1;
    const foundContacts = await this.contactRepository.find({
      where: { user: userId, isDeleted: false },
      order: { name: sort === 'ASC' ? -1 : 1 },
      take: limit || 10,
      skip: offset || 0,
    });

    const mappedContacts: Promise<ShowContactDTO>[] = foundContacts.map(
      async (contact) => {
        const mappedContacts: ShowContactDTO = {
          id: contact.id,
          name: contact.name,
          phone: contact.phone,
          address: contact.address,
          username: (await contact.user).username,
        };

        return mappedContacts;
      },
    );

    return { contacts: await Promise.all(mappedContacts), count: cnt };
  }

  public async createContact(
    user: User,
    body: CreateContactDTO,
  ): Promise<ShowContactDTO> {
    const contact = this.contactRepository.create(body);
    contact.user = Promise.resolve(user);

    const createdContact = await this.contactRepository.save(contact);

    return plainToClass(ShowContactDTO, createdContact, {
      excludeExtraneousValues: true,
    });
  }

  public async deleteContact(user: User, contactId: number): Promise<{ msg }> {
    const contact = await this.contactRepository.findOne({
      where: {
        id: contactId,
        user: user,
        isDeleted: false,
      },
    });

    contact.isDeleted = true;
    await this.contactRepository.save(contact);
    return { msg: 'Contact Deleted' };
  }
}
