import { AuthGuard } from '@nestjs/passport';
import {
  Controller,
  Post,
  HttpCode,
  HttpStatus,
  Req,
  Param,
  ParseIntPipe,
  Body,
  UseGuards,
  Get,
} from '@nestjs/common';
import { ContactService } from './contact.service';
import { ShowContactDTO } from './models/show-contact.dto';
import { CreateContactDTO } from './models/create-contact.dto';

@Controller('contact')
export class ContactController {
  public constructor(private readonly contactService: ContactService) {}

  @Get('/contacts/:userId/:sort/:limit/:offset')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async getAllUserContacts(
    @Param('userId') userId: string,
    @Param('sort') sort: string,
    @Param('limit') limit: number,
    @Param('offset') offset: number,
  ): Promise<ShowContactDTO[]> {
    const userContacts: ShowContactDTO[] =
      await this.contactService.getUserContacts(userId, sort, limit, offset);

    return userContacts;
  }

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.CREATED)
  public async addNewContact(
    @Req() req: any,
    @Body() body: CreateContactDTO,
  ): Promise<{ msg }> {
    await this.contactService.createContact(req.user, body);

    return { msg: 'Contact Added' };
  }

  @Post('/:contactId')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async deleteContact(
    @Req() req: any,
    @Param('contactId', ParseIntPipe) contactId: number,
  ): Promise<{ msg }> {
    await this.contactService.deleteContact(req.user, contactId);

    return { msg: 'Review Deleted' };
  }
}
