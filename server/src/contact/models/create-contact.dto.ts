import { Length } from 'class-validator';

export class CreateContactDTO {
  @Length(1, 30)
  public name: string;

  @Length(1, 10)
  public phone: string;

  @Length(1, 80)
  public address: string;
}
