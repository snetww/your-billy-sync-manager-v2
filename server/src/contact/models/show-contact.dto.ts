import { Expose } from 'class-transformer';

export class ShowContactDTO {
  @Expose()
  public id: number;

  @Expose()
  public name: string;

  @Expose()
  public phone: string;

  @Expose()
  public address: string;

  @Expose()
  public username: string;
}
