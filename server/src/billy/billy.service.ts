import { Contact } from 'src/database/entities/contact.entity';
import { Product } from './../database/entities/product.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { HttpService } from '@nestjs/axios';
import { baseUrlBilly, headersRequestBilly } from 'src/common/billyCredentials';
import { map } from 'rxjs';

@Injectable()
export class BillyService {
  constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
    @InjectRepository(Contact)
    private readonly contactRepository: Repository<Contact>,
    private readonly httpService: HttpService,
  ) {}
  public async addToBilly(
    userId: string,
    item: string,
    obj: any,
  ): Promise<void> {
    const result = await this.httpService
      .post(`${baseUrlBilly}/${userId}/${item}`, obj, {
        headers: headersRequestBilly,
      })
      .pipe(map((response) => response.data));
  }
  public async syncProductsAndContacts(userId: string): Promise<any> {
    const products = await this.productRepository.find({
      where: { user: userId, isDeleted: false },
    });

    const contacts = await this.contactRepository.find({
      where: { user: userId, isDeleted: false },
    });

    for await (const product of products) {
      await this.addToBilly(userId, 'products', product);
    }

    for await (const contact of contacts) {
      await this.addToBilly(userId, 'contacts', contact);
    }
    return { msg: 'OK' };
  }
}
