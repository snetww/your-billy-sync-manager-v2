import { HttpModule, HttpService } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Contact } from 'src/database/entities/contact.entity';
import { Product } from 'src/database/entities/product.entity';
import { BillyController } from './billy.controller';
import { BillyService } from './billy.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Product]),
    TypeOrmModule.forFeature([Contact]),
    HttpModule,
  ],
  controllers: [BillyController],
  providers: [BillyService],
})
export class BillyModule {}
