import {
  Controller,
  HttpCode,
  HttpStatus,
  Get,
  Param,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BillyService } from './billy.service';
@Controller('billy')
export class BillyController {
  constructor(private readonly billyService: BillyService) {}
  @Get('/add/:userId')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async syncProductsAndContacts(
    @Param('userId') userId: string,
  ): Promise<any> {
    return await this.billyService.syncProductsAndContacts(userId);
  }
}
