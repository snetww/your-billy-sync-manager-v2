export interface LoginStatus {
  message: string;
  accessToken: any;
  userId: string;
}
