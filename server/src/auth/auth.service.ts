import { UserLoginDTO } from '../user/models/user-login.dto';
import { UserService } from './../user/user.service';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './models/jwt-payload';
import { CreateUserDTO } from 'src/user/models/user-create.dto';
import { RegistrationStatus } from './models/registration-status';
import { UserDTO } from 'src/user/models/user.dto';
import { LoginStatus } from './models/login-status';
import { JwtConstants } from './constants/jwt-constants';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly userService: UserService,
  ) {}

  private readonly _blacklist: string[] = [];
  async register(
    userRegCredentials: CreateUserDTO,
  ): Promise<RegistrationStatus> {
    let status: RegistrationStatus = {
      success: true,
      message: 'user registered',
    };
    try {
      await this.userService.createUser(userRegCredentials);
    } catch (err) {
      status = {
        success: false,
        message: err,
      };
    }
    return status;
  }

  async login(userLoginCredentials: UserLoginDTO): Promise<LoginStatus> {
    const user: UserDTO = await this.userService.findByLoginCredentials(
      userLoginCredentials,
    );
    const token = await this._createToken(user);
    return {
      message: `Welcome, ${user.username}! Your access expires in ${JwtConstants.expiry}`,
      ...token,
      userId: user.id,
    };
  }

  async logout(token: string): Promise<{ msg: string }> {
    this._blacklist.push(token);

    return {
      msg: `Logout succesful`,
    };
  }

  async validateUser(payload: JwtPayload): Promise<UserDTO> {
    const user: UserDTO = await this.userService.findByPayload(payload);
    if (!user) {
      throw new HttpException(
        `Invalid or expired token`,
        HttpStatus.UNAUTHORIZED,
      );
    }
    return user;
  }

  private async _createToken(user: UserDTO): Promise<{ accessToken: string }> {
    const userCredentials: JwtPayload = { ...user };
    const accessToken = await this.jwtService.signAsync(userCredentials);
    return {
      accessToken,
    };
  }
}
