import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';

import { User } from './user.entity';

@Entity('contacts')
export class Contact {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column('nvarchar', { length: 30 })
  public name: string;

  @Column('nvarchar')
  public phone: string;

  @Column('nvarchar', { length: 80 })
  public address: string;

  @Column({ nullable: false, default: false })
  public isDeleted: boolean;

  @ManyToOne(() => User, (user) => user.contacts)
  public user: Promise<User>;
}
