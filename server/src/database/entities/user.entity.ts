import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';

import { Product } from './product.entity';
import { Contact } from './contact.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column('nvarchar', { length: 12 })
  public username: string;

  @Column('nvarchar')
  public password: string;

  @Column('nvarchar', { length: 36 })
  public email: string;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public lastLogin: Date;

  @OneToMany(() => Product, (product) => product.user)
  public products: Promise<Product[]>;

  @OneToMany(() => Contact, (contact) => contact.user)
  public contacts: Promise<Contact[]>;
}
