import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';

import { User } from './user.entity';

@Entity('products')
export class Product {
  @PrimaryGeneratedColumn('increment')
  public id: string;

  @Column('nvarchar', { length: 40 })
  public name: string;

  @Column('nvarchar')
  public vendor: string;

  @Column('nvarchar', { length: 20 })
  public version: string;

  @Column({ nullable: false, default: false })
  public isDeleted: boolean;

  @ManyToOne(() => User, (user) => user.products)
  public user: Promise<User>;
}
