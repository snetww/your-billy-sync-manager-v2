import { UserService } from './user.service';
import {
  Controller,
  Post,
  HttpCode,
  HttpStatus,
  Body,
  Get,
  Param,
} from '@nestjs/common';
import { CreateUserDTO } from './models/user-create.dto';
import { UserDTO } from './models/user.dto';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('/:userId')
  @HttpCode(HttpStatus.OK)
  public async getUser(@Param('userId') userId: string): Promise<any> {
    return await this.userService.getUser(userId);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  public async addUser(@Body() body: CreateUserDTO): Promise<UserDTO> {
    return await this.userService.createUser(body);
  }
}
