import { Expose } from 'class-transformer';

export class UserDTO {
  @Expose()
  public id: string;

  @Expose()
  public username: string;

  @Expose()
  public email: string;
}
