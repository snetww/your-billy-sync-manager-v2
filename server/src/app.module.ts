import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductModule } from './product/product.module';
import { ContactModule } from './contact/contact.module';
import { PassportModule } from '@nestjs/passport';
import { AuthModule } from './auth/auth.module';
import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { BillyModule } from './billy/billy.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    UserModule,
    AuthModule,
    PassportModule,
    ProductModule,
    ContactModule,
    BillyModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
