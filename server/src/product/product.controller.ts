import { CreateProductDTO } from './models/create-product.dto';
import { ShowProductDTO } from './models/show-product.dto';
import {
  Controller,
  Post,
  UseGuards,
  HttpCode,
  HttpStatus,
  Body,
  Req,
  Param,
  Get,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ProductService } from './product.service';

@Controller('products')
export class ProductController {
  public constructor(private readonly productService: ProductService) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.CREATED)
  public async createNewProduct(
    @Body() body: CreateProductDTO,
    @Req() req: any,
  ): Promise<ShowProductDTO> {
    return await this.productService.createProduct(body, req.user);
  }

  @Post('/:productId')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async deleteProduct(
    @Param('productId') productId: number,
  ): Promise<{ msg: 'Product Deleted' }> {
    await this.productService.deleteProduct(productId);

    return { msg: 'Product Deleted' };
  }

  @Get('/:userId/:sort/:limit/:offset')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async getAllUserProducts(
    @Param('userId') userId: string,
    @Param('sort') sort: string,
    @Param('limit') limit: number,
    @Param('offset') offset: number,
  ): Promise<any> {
    return await this.productService.getAllProducts(
      userId,
      sort,
      limit,
      offset,
    );
  }
}
