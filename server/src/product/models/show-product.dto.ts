import { Expose } from 'class-transformer';

export class ShowProductDTO {
  @Expose()
  public id: number;

  @Expose()
  public name: string;

  @Expose()
  public vendor: string;

  @Expose()
  public version: string;
}
