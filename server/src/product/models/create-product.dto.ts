import { Length, IsNotEmpty } from 'class-validator';

export class CreateProductDTO {
  @IsNotEmpty()
  @Length(2, 40)
  public name: string;

  @IsNotEmpty()
  @Length(2, 40)
  public vendor: string;

  @IsNotEmpty()
  @Length(2, 20)
  public version: string;
}
