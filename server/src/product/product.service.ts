import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from '../database/entities/product.entity';
import { Repository } from 'typeorm';
import { User } from '../database/entities/user.entity';
import { CreateProductDTO } from './models/create-product.dto';
import { UserDTO } from 'src/user/models/user.dto';
import { ShowProductDTO } from './models/show-product.dto';
import { plainToClass } from 'class-transformer';

@Injectable()
export class ProductService {
  public constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
  ) {}

  public async createProduct(
    product: CreateProductDTO,
    user: UserDTO,
  ): Promise<ShowProductDTO> {
    const productEntity: Product = this.productRepository.create(product);
    const foundUser: User = await this.usersRepository.findOne({
      username: user.username,
    });

    productEntity.user = Promise.resolve(foundUser);
    await this.productRepository.save(productEntity);

    return plainToClass(ShowProductDTO, productEntity, {
      excludeExtraneousValues: true,
    });
  }

  public async deleteProduct(productId: number): Promise<void> {
    const foundProduct = await this.productRepository.findOne({
      where: { id: productId },
    });

    foundProduct.isDeleted = true;
    await this.productRepository.save(foundProduct);
  }

  public async getAllProducts(
    userId: string,
    sort: string,
    limit: number,
    offset: number,
  ): Promise<any> {
    const cnt = await this.productRepository.count({ where: { user: userId } });
    offset = offset - 1 < 0 ? 0 : offset - 1;
    const foundProducts = await this.productRepository.find({
      where: { user: userId },
      order: { name: sort === 'ASC' ? -1 : 1 },
      take: limit || 10,
      skip: offset || 0,
    });

    return { products: foundProducts, count: cnt };
  }
}
